import crypto from 'node:crypto';

enum CurrencyEnum {
    USD = 'USD',
    UAH = 'UAH'
}

class Transaction {
    id: string;
    amount: number;
    currency: CurrencyEnum;

    constructor(amount: number, currency: CurrencyEnum) {
        this.id = crypto.randomUUID();
        this.amount = amount;
        this.currency = currency;
    }
}

class Card  {
    transactions: Transaction[];

    constructor() {
        this.transactions = [];
    }

    // Overload signatures
    addTransaction(transaction: Transaction): string;
    addTransaction(currency: CurrencyEnum, amount: number): string;


    // Implementation signature
    addTransaction(arg1: CurrencyEnum | Transaction, arg2?: number): string {
        if (typeof arg1 === 'object') {
            // якщо переданий об'єкт Transaction
            const transaction = arg1 as Transaction;
            this.transactions.push(transaction);
            return transaction.id;
        } else if (typeof arg1 === 'string' && typeof arg2 === 'number') {
            // якщо передані валюта та сума
            const currency = arg1 as CurrencyEnum;
            const amount = arg2 as number;
            const transaction = new Transaction(amount, currency);
            this.transactions.push(transaction);
            return transaction.id;
        }
        throw new Error('Invalid arguments');
    }

    // addTransaction(transaction: Transaction): string {
    //     this.transactions.push(transaction);
    //     return transaction.id;
    // }

    // addTransaction(currency: CurrencyEnum, amount: number): string {
    //     const transaction = new Transaction(amount, currency);
    //     this.transactions.push(transaction);
    //     return transaction.id;
    // }

    getTransaction(id: string) {
        return this.transactions.find(transaction => transaction.id === id);
    }

    getBalance(currency: CurrencyEnum): number {
        return this.transactions.reduce((total, transaction) => {
            if (transaction.currency === currency) {
                return total + transaction.amount;
            } else {
                return total;
            }
        }, 0);
    }

}


const card1 = new Card();

const transactionId1 = card1.addTransaction(new Transaction(100, CurrencyEnum.USD));
const transactionId2 = card1.addTransaction(CurrencyEnum.UAH, 200);

console.log(card1.getTransaction(transactionId1));
console.log(card1.getTransaction(transactionId2));

console.log(card1.getBalance(CurrencyEnum.UAH));
